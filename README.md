# CMGPSLogger
A small project with an atmega 328 microcontroller to store the gps (and more) data into a sd card.
This was my second project from 2012-2013, to save GPS coordinates into a sd-card.
In the first version i build the circuit and make a mistake with the power controller, this was only a step up controller MAX1674, but it is not a buck boost converter. Why is that important -> in this and other versions, we have implement a MAX1811 lion loader, to load the battery (lion battery) over the micro usb connector. But the MAX1811 give 4.2 volts to the battery and the 4.2 volts will also connected to the MAX1674, and this power controller will pass through the 4.2 volts to other components, they will only use and needed 3.3 volts. Oh, big mistake, the sd -card if there is one insert, will directly damaged. Thats wy i will produce a new logger with a buck boost converter.

----

#### Technical Informations
* atmega 328 microcontroller @16Mhz
* EA DOG XL 160 Display
* SD-Card holder
* MAX1811 Lion loader
* Buck Boost Converter (?)
* EM406 GPS module
